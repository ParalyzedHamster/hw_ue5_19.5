﻿#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void makeSound() const = 0;
};

class Killer_Cat : public Animal
{
public:
    void makeSound() const override
    {
        cout << "Killer Cat- You will die in the night!\n";
    }
};

class Tank_Dog : public Animal
{
public:
    void makeSound() const override
    {
        cout << "Tank Dog- Wof! Wof! No one will pass! \n";
    }
};
class Water_Magic_guinea_pig : public Animal
{
public:
    void makeSound() const override
    {
        cout << "Water Magic guinea pig- Catch the Waterball! " << endl;
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Killer_Cat();
    animals[1] = new Tank_Dog();
    animals[2] = new Water_Magic_guinea_pig();

    for (Animal* a : animals)
        a->makeSound();
}